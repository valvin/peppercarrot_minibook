#!/usr/bin/python
import requests
import re
import sys
import os
import zipfile


class EpisodeDownloader:
    BASE_URL = "https://www.peppercarrot.com/0_sources/"
    DOWNLOAD_DIR = "./pages"

    def download_file(self,url,path):
        p = re.compile('[a-z]{2}_Pepper-and-Carrot_by-David-Revoy_E[0-9]{2}P([0-9]{2}).jpg')
        remote_filename = url.split('/')[-1]
        m =p.match(remote_filename) 
        if m :
            local_filename = m.group(1) + '.jpg'
        else :
            local_filename = '__.jpg'
            
        r = requests.get(url, stream=True)
        with open(path + "/" + local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
        return local_filename


    def zip_dir(self,path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file))


    def create_cbz_file(self,dir, file_name):
        zipf = zipfile.ZipFile(file_name, 'w', zipfile.ZIP_DEFLATED)
        zip_dir(dir, zipf)
        zipf.close()


    def episodes_list(self):
        r = requests.get(self.BASE_URL + ".episodes-list.md", stream=True)
        return r.content.decode().split("\n")


    def download_episode(self,episode, locale,cbz = False):
        print("looking for " + episode + " for locale " + locale)
        file_regex = "<a href=\"" + locale + "(.+?)\">"
        pattern = re.compile(file_regex)
        r = requests.get(self.BASE_URL + episode + "/hi-res/")
        html = r.content.decode()
        files = re.findall(pattern, html)
        for file in files:
            file_url = self.BASE_URL + episode + "/hi-res/" + locale + file
            print("downloading : " + file_url)
            self.download_file(file_url, self.DOWNLOAD_DIR)
        if cbz :
            self.create_cbz_file(self.DOWNLOAD_DIR, locale + "_peppercarrot.cbz")
    

    def clean_download_dir(self):
        files = os.listdir(self.DOWNLOAD_DIR)
        for f in files:
            print('deleting {}'.format(f))
            os.remove(self.DOWNLOAD_DIR + '/' + f)



if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("episode_downloader.py <episodes: \"1,2\"> <loaclae \"en\">")
    
    ed = EpisodeDownloader()
    episodes = ed.episodes_list()
    episode_to_download = sys.argv[1].split(",")
    ed.clean_download_dir()
    for ep in episode_to_download:
        if int(ep) - 1 <= len(episodes):
            ed.download_episode(episodes[int(ep) - 1], sys.argv[2])
        else:
            print("This episode is not yet available ;-)")
