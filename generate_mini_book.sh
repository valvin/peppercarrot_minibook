#!/bin/sh
if [ $# -eq 2 ]; then
  echo "************ Download episode ******************"
  ./episode_downloader.py $1 $2
  echo "************ Generating Minibook PDF ***********"
  echo "  Deleting previous generating PDF"
  rm ./minibook_A4.pdf
  echo "  Scribus is opening but closes automatically after a while "
  notify-send -u normal -t 10000 "IGNORE Scribus opening!" "It will close itself in few seconds after it generates the PDF"
  scribus-ng -g -ns -py to-pdf.py -- minibook_A4.sla 2>/dev/null 
  echo "  PDF is now available in ./minibook_A4.pdf"
  echo "************************************************"
else
  echo "./generatePDF.sh [episode_numbe] [locale]"
fi
