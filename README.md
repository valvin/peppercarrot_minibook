# Pepper&Carrot Minibook

This project allowes to print an episode on a A4 piece of paper.

It can work with all episodes with less than 8 pages but it is recommended to use it with silent episodes (5, 10, 15, 20 ...)

![](doc/scribus_preview_template.png)

## Automatic method

Run `./generate_mini_book.sh [episode_number] [locale]`

Example : `./generate_mini_book.sh 15 en`

The script downloads episode then generate a A4 PDF using scribus. Scribus is opening then close after few seconds.

## Manual method

### Step 1 - downlaod an episode

Using [episode_downloader](https://framagit.org/valvin/peppercarrot_episode_downloader) script that it has been a little bit adapted to this project

currently there is a small which requires to create the folder `./pages`

```
mkdir ./pages
```


example to download episode 15 in french

```
./episode_downloader.py 15 fr
```

### Step 2 - Scribus

open the file `minibook_A4.sla`

![](doc/scribus_preview_ep15.png)

then File / Export / Save as PDF

Ignore Errors

And Save

## Print and fold and cut

This video explains it better than words : [here](https://www.youtube.com/watch?v=21qi9ZcQVto)

The result should be something like that : [video on peertube](https://peertube.valvin.fr/videos/watch/fef4ca2e-1711-4694-a087-2da8c625c8d9)
